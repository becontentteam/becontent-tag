<?php

namespace becontent\tags\presentation;
use becontent\tags\presentation\Tag as Tag;
use becontent\tags\presentation\TagLibrary as TagLibrary;

/**
	 * @access public
	*/
	class TagLibrariesFactory {

		public $supportedTaglibraries;
		private static $instance;
		private static $instanceWasBuilt=false;

		public static function getInstance()
		{
			if(!isset(self::$instance))
			{
				self::$instance=new TagLibrariesFactory();
			}
			return self::$instance;
		}

		public function __construct()
		{
			$this->supportedTaglibraries=array();
		}

		/**
		 * Used by classloaders to inject a tagLibrary in the factory 
		 * @param unknown $nodeKey
		 * @param unknown $tagNode
		 */
		public function injectTag($nodeKey,$nodeTag)
		{
			$this->supportedTaglibraries[$nodeKey]=$nodeTag;
		}
		
		public function injectTags($supportedTagLibraries)
		{
			$this->supportedTaglibraries=array_merge($this->supportedTaglibraries, $supportedTagLibraries);
		}
		
		/**
		 * @access public
		 * @param aKey
		 * @ParamType aKey
		 */
		public function tag($tagKey,$parameters) {
			$pars=get_object_vars(json_decode($parameters));
			$tagRoute= explode(":", $tagKey);
			$thisNodeKey=$tagRoute[0];
			unset($tagRoute[0]);
			$tagRoute = array_values($tagRoute);
			return $this->supportedTaglibraries[$thisNodeKey]->doIt($tagRoute,$pars);
		}
	}
	?>