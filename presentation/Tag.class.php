<?php
namespace becontent\tags\presentation;
use becontent\tags\presentation\TagLibrariesFactory as TagLibrariesFactory;
/**
 * @access public
 */
class Tag implements \JsonSerializable {

	
	private $filepath;
	
	
	/**
	 * @access public
	 * @param aExtrinsicState
	 * @ParamType aExtrinsicState 
	 */
	
	public function __construct($structureAsArray=null,$fileSystemElement=null,$mode="as_array")
	{
		/**
		 * This is the leaf of the tree so nothing to do here,
		 * see TagLibrary implementation of this method
		 */
		$this->filepath=$fileSystemElement;
	}
	
	public function doIt($tagRoute,$parameters){}
	
	public function addTag($aKey, $tag){}
	
	public function removeTag($aKey){}
	
	
	public function jsonSerialize()
	{
		$turnback=array();
		$turnback["classname"]=get_class($this);
		$turnback["classpath"]=$this->filepath;
		return $turnback;
	}
	
	/**
	 *
	 * @param unknown $fileSystemElement
	 */
	protected function buildFromFileSystem($fileSystemElement)
	{
		/**
		 * This is the leaf of the tree so nothing to do here, 
		 * see TagLibrary implementation of this method
		 */
	}
	
	/**
	 *
	 * @param unknown $structureAsArray
	 */
	protected function buildFromArray($structureAsArray)
	{
		/**
		 * This is the leaf of the tree so nothing to do here, 
		 * see TagLibrary implementation of this method
		 */
	}
}
?>